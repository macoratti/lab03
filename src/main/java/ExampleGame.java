import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExampleGame {

	final static Logger logger = LoggerFactory.getLogger(ExampleGame.class);
	
	public static void main(String[] args) {
		Game game = new Game();
		
		Random rnd = new Random();
		
		for (int i=0; i<21; ++i) {
			game.roll(rnd.nextInt(11));
			logger.info("{}", game.score());
		}
	}

}
