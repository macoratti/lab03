public class Game {

	private int[] rolls;
	private int roll;
	
	public Game() {
		rolls = new int[21];
		roll = 0;
	}
	
	private boolean isStrike(int pos) {
		return rolls[pos] == 10;
	}
	
	private boolean isSpare(int pos) {
		return (rolls[pos] + rolls[pos+1]) == 10;
	}
	
	private int frameTotal(int pos) {
		return rolls[pos] + rolls[pos+1];
	}
	
	private int spareBonus(int pos) {
		return rolls[pos+2];
	}
	
	private int strikeBonus(int pos) {
		return rolls[pos+1] + rolls[pos+2];
	}
	
	public void roll(int pins) {
		rolls[roll] = pins;
		roll += 1;
	}
	
	public int score() {
		int score = 0, halfFrame = 0;
		for (int frame=0; frame<10; ++frame) {
			if (isStrike(halfFrame)) {
				score += 10 + strikeBonus(halfFrame);
				halfFrame += 1;
			} else if (isSpare(halfFrame)) {
				score += 10 + spareBonus(halfFrame);
				halfFrame += 2;
			} else {
				score += frameTotal(halfFrame);
				halfFrame += 2;
			}
		}
		return score;
	}
	
}
